//
// Created by sorina on 26.03.2020.
//

#ifndef JOSEPHUS_LIST_H
#define JOSEPHUS_LIST_H

#include <iostream>
#include "Node.h"

class List {
private:
    Node *root = nullptr;
    int size = 0;
public:
    List() {}

    virtual ~List() {
        delete root;
    }

    Node *getRoot() const {
        return root;
    }

    void setRoot(Node *root) {
        List::root = root;
    }

    int getSize() const {
        return size;
    }

    void increaseSize() {
        List::size++;
    }

    void removeNode(Node **first, Node **second) {
        (*first)->setNext((*second)->getNext());
        (*second) = (*first)->getNext();
    }

    void push(Node **head_reference, int data) {
        Node *ptr1 = new Node(data, *head_reference);
        Node *temp = *head_reference;

        if (*head_reference != nullptr) {
            while (temp->getNext() != *head_reference)
                temp = temp->getNext();
            temp->setNext(ptr1);
        } else {
            ptr1->setNext(ptr1);
        }

        *head_reference = ptr1;
    }

    void printThirdElement(Node *head) {
        std::cout << "the third person from the given one is on position: "
                  << head->getNext()->getNext()->getNext()->getData()
                  << "\n";
    }

    void printList(Node *head) {
        Node *temp = head;
        if (head != nullptr) {
            do {
                std::cout << temp->getData() << " ";
                temp = temp->getNext();
            } while (temp != head);
        }
    }


    void insertAfter(Node *prev_node, int new_data) {
        if (prev_node == nullptr) {
            std::cout << "the previous node can't be NULL";
            return;
        }

        Node *new_node = new Node(new_data, prev_node->getNext());
        prev_node->setNext(new_node);
    }
};

#endif //JOSEPHUS_LIST_H
