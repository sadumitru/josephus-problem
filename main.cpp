#include <iostream>
#include "Josephus.h"
//#include <chrono>

int main(int argc, char *argv[]) {
    int nrOfPersons, period, startPosition;
    std::cout << "the number of persons:\n ";
    std::cin >> nrOfPersons;
    std::cout << "the person to start from: \n";
    std::cin >> startPosition;
    std::cout << "the period for elimination:\n";
    std::cin >> period;
    Josephus j(nrOfPersons, startPosition, period);
    j.runGame();

//    Josephus j(atoi(argv[1]), atoi(argv[2]), atoi(argv[3]));
//    auto t1 = std::chrono::high_resolution_clock::now();
//    j.runGame();
//    auto t2 = std::chrono::high_resolution_clock::now();
//    auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
//    std::cout << " " << duration << '\n';
//    return 0;
}
