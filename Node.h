//
// Created by sorina on 26.03.2020.
//

#ifndef JOSEPHUS_NODE_H
#define JOSEPHUS_NODE_H


class Node {
private:
    int data = 0;
    Node *next = nullptr;

public:
    Node(int data) : data(data) {}

    Node(int data, Node *next) : data(data), next(next) {}

    virtual ~Node() {
        delete next;
    }

    int getData() const {
        return data;
    }

    void setData(int data) {
        Node::data = data;
    }

    Node *getNext() const {
        return next;
    }

    void setNext(Node *pNode) {
        Node::next = pNode;
    }
};


#endif //JOSEPHUS_NODE_H
