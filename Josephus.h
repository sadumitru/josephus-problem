//
// Created by sorina on 26.03.2020.
//

#ifndef JOSEPHUS_JOSEPHUS_H
#define JOSEPHUS_JOSEPHUS_H

#include "Node.h"
#include "List.h"

class Josephus {
private:
    int persons = 0, start = 0, period = 0;
    List *l = new List();

    void makeList() {
        Node *temp = new Node(1);
        Node* x = temp;

        if(start == 1){
            l->setRoot(temp);
        }

        for(int j = 2; j <= persons; j++){
            temp->setNext(new Node(j));
            if(j == start){
                l->setRoot(temp->getNext());
            }
            temp = temp->getNext();
            l->increaseSize();
        }

        temp->setNext(x);

    }

public:
    Josephus(int persons, int start, int period) : persons(persons), start(start), period(period) {}

    void setRules(int persons, int start, int period) {
        this->persons = persons;
        this->start = start;
        this->period = period;
    }

    void runGame() {
        makeList();

        Node *ptr1 = l->getRoot(), *ptr2 = l->getRoot();

        while (ptr1->getNext() != ptr1 && ptr1->getNext() != nullptr) {
            int count = 1;

            while (count != period) {
                ptr2 = ptr1;
                ptr1 = ptr1->getNext();
                count++;
            }

            l->removeNode(&ptr2, &ptr1);
        }

//        std::cout << "the last person left standing is on position: " << ptr1->getData() << '\n';
        std::cout << ptr1->getData();
    }
};

#endif //JOSEPHUS_JOSEPHUS_H
